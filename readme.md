## Credit where credit is due

This is from another startpage I found [here](https://github.com/Jaredk3nt/homepage) which I thought looked cool
but didn't suite my taste completely. Feel free to edit this or not I really
don't mind.

## Changes from the original

- Search Engine

Using startpage instead of google

- Links/Weather Data

Changed to links I actually use/care about.
- Font

Font for most things is `Fixedsys Excelsior 3.01 Regular` you can find that font and others
that I think are really nice [here](https://int10h.org/).

Font for the clock is `FreeMono`.

Replace the background with whatever you want just replace the name with
`back.jpg`. 

- Boxes 

Are slightly transparent and blackened to be a e s t h e t i c.

![alt text](preview.png)